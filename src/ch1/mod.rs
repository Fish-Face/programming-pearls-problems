use rand::prelude::*;
// use bitvec::prelude::*;
use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead, BufWriter, Write};
//use std::mem::size_of;
// use std::ops::Index;

use anyhow::Result;

const SIZE: usize = 1_000_000;
const MAX: usize = 10_000_000;
const BITS_PER_USIZE: usize = 1; //size_of::<usize>() * 8;

#[derive(Debug)]
struct MyBitVec {
    //max: usize,
    storage: Vec<usize>,
}

impl MyBitVec {
    fn new(max: usize) -> Self {
        let storage = vec![0; max / BITS_PER_USIZE + 1];
        Self {
            //max: max,
            storage: storage,
        }
    }
    fn get(&self, pos: usize) -> bool {
        let q = pos / BITS_PER_USIZE;
        let r = pos % BITS_PER_USIZE;
        (self.storage[q] & 1 << r) != 0
    }
    fn set_true(&mut self, pos: usize) {
        let q = pos / BITS_PER_USIZE;
        let r = pos % BITS_PER_USIZE;
        self.storage[q] |= 1 << r;
    }
    fn iter_ones(&self) -> impl Iterator<Item=usize> + '_ {
        self.storage.iter().enumerate().map(
            |(i, v)| (0..BITS_PER_USIZE).filter_map(
                move |b| match v & 1 << b {
                    0 => None,
                    _ => Some(i * BITS_PER_USIZE + b),
                }
            )
        ).flatten()
    }
}

// Cannot use because the derived bool isn't owned by anything and Index always returns a reference
// actual BitVec gets around this by returning a reference
// impl Index<usize> for MyBitVec {
//     type Output = bool;
//     fn index(&self, i: usize) -> &Self::Output {
//         let q = i / BITS_PER_USIZE;
//         let r = i % BITS_PER_USIZE;
//         &((self.storage[q] & 1 << r) != 0)
//     }
// }

fn generate(path: &Path, n: usize, max: usize) -> Result<()> {
    let mut used = MyBitVec::new(max);
    let mut rng = rand::thread_rng();
    let mut count = n;
    let mut file = File::create(path)?;
    while count > 0 {
        let v = rng.gen_range(0..max);
        if used.get(v) {
            continue;
        }
        used.set_true(v);
        count -= 1;
        file.write_fmt(format_args!("{}\n", v))?;
    }

    Ok(())
}

fn bitvec_sort(in_path: &Path, out_path: &Path, max: usize) -> Result<()> {
    let reader = BufReader::new(File::open(in_path)?);
    // let mut storage = bitvec![0; max];
    let mut storage = MyBitVec::new(max);
    for line in reader.lines() {
        let value = line?.parse::<usize>()?;
        storage.set_true(value);
    }

    let mut writer = BufWriter::new(File::create(out_path)?);
    for value in storage.iter_ones() {
        writer.write_fmt(format_args!("{}\n", value))?
    }
    Ok(())
}

fn bitvec_multipass_sort(in_path: &Path, out_path: &Path, max: usize) -> Result<()> {
    Ok(())
}

pub fn main(problem: u32, args: &Vec<String>) {
    match problem {
        3 => {
            let in_path = args.get(0).expect("Expected path for input file");
            let out_path = args.get(1).expect("Expected path for output file");
            bitvec_sort(Path::new(&in_path), Path::new(&out_path), MAX).unwrap();
        },
        4 => {
            let path = args.get(0).expect("Expected path for generated file");
            generate(Path::new(&path), SIZE, MAX).unwrap();
        },
        5 => {
            let in_path = args.get(0).expect("Expected path for input file");
            let out_path = args.get(1).expect("Expected path for output file");
            bitvec_multipass_sort(Path::new(&in_path), Path::new(&out_path), MAX).unwrap();
        },
        _ => panic!("Unknown/not implemented problem {}", problem),
    }
}
