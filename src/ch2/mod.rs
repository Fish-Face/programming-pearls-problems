use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::HashMap;

use maplit::hashmap;
use anyhow::Result;
use lazy_static::lazy_static;

use crate::utils::time;

// Find missing integer from 4 billion 32 bit ints (= 16GB)
// in RAM: load into 4 billion long bit array (=0.5 GB), scan until hitting a missing number
// on disk: populate a 0.5 GB binary file in the same way, then scan it sequentially.
//
// Rotate a vector: [abcdefghi] -2-> [cdefghiab]

fn vec_rotate_juggle(input: &mut Vec<u64>, i: usize) {
    let len = input.len();
    let i = i % len;
    let mut tmpa: u64;
    let mut start: usize = i;
    let mut src = start;
    let mut dst: usize = 0;
    tmpa = input[dst];
    for _ in 0..len {
        input[dst] = input[src];
        //println!("{src} -> {dst} {input:?} {tmpa}");
        src = (src + i) % len;
        if src == start {
            // This block is executed gcd(i, n) times
            input[dst] = tmpa;
            src = (src + 1) % len;
            start = src;
            dst = ((src as isize - i as isize).rem_euclid(len as isize)) as usize;
            tmpa = input[dst];
        }
        dst = ((src as isize - i as isize).rem_euclid(len as isize)) as usize;
    }
}

fn vec_slice_swap(a: &mut [u64], b: &mut [u64]) {
    assert_eq!(a.len(), b.len());
    let mut tmp: u64;
    for i in 0..a.len() {
        tmp = a[i];
        a[i] = b[i];
        b[i] = tmp;
    }
}

fn vec_rotate_swap(input: &mut [u64], i: usize) {
    let len = input.len();
    if i >= len {
        return;
    }
    let (l, r) = input.split_at_mut(i);
    if i <= len / 2 {
        let (_, rb) = r.split_at_mut(len - 2 * i);
        vec_slice_swap(l, rb);
        vec_rotate_swap(&mut input[0..len - i], i);
    } else {
        let (la, _) = l.split_at_mut(len - i);
        vec_slice_swap(la, r);
        vec_rotate_swap(&mut input[0..len - i], i);
    }
}

fn slice_reverse(input: &mut [u64]) {
    input.reverse();
    /*let len = input.len();
    let mut tmp: u64;
    for i in 0..input.len() / 2 {
        tmp = input[i];
        input[i] = input[len - i - 1];
        input[len - i - 1] = tmp;
    }*/
}

fn vec_rotate_reverse(input: &mut [u64], i: usize) {
    slice_reverse(input.split_at_mut(i).0);
    slice_reverse(input.split_at_mut(i).1);
    slice_reverse(input);
}

// Find all sets of anagrams
// for w in words:
//      anagrams[sorted(w)].append(w)
lazy_static! {
    static ref KEYMAP: HashMap<char, char> = hashmap!{
        'a' => '2',
        'b' => '2',
        'c' => '2',
        'd' => '3',
        'e' => '3',
        'f' => '3',
        'g' => '4',
        'h' => '4',
        'i' => '4',
        'j' => '5',
        'k' => '5',
        'l' => '5',
        'm' => '6',
        'n' => '6',
        'o' => '6',
        'p' => '7',
        'q' => '7',
        'r' => '7',
        's' => '7',
        't' => '8',
        'u' => '8',
        'v' => '8',
        'w' => '9',
        'x' => '9',
        'y' => '9',
        'z' => '9',
    };
}

fn leskify(name: &str) -> Option<String> {
    name.chars().map(|c| KEYMAP.get(&c)).collect::<>()
}

type Directory = HashMap<String, Vec<String>>;

fn lesk_directory(path: &Path) -> Result<Directory> {
    let reader = BufReader::new(File::open(path)?);
    let names = reader.lines().filter_map(|line| line.ok());

    let mut result: Directory = HashMap::new();
    for name in names {
        let l = leskify(&name);
        if let Some(l) = l {
            result.entry(l).or_insert(vec![]).push(name.to_string());
        }
    }
    Ok(result)
}

fn lesk_duplicates(directory: &Directory) -> Result<Vec<Vec<String>>> {
    Ok(directory.into_iter().filter_map(|(_, v)| match v.len() {
        0 => None,
        1 => None,
        _ => Some(v.clone()),
    }).collect::<>())
}

pub fn main(problem: u32, args: &Vec<String>) {
    match problem {
        4 => {
            let l = args.get(0).expect("Need a length").parse::<u64>().expect("Couldn't parse length as u64");
            let i = args.get(1).expect("Need a length to rotate by").parse::<usize>().expect("Couldn't parse rotation length as usize");
            //let mut input = args_iter.map(|s| s.parse::<u64>().expect("Expected integers")).collect::<Vec<u64>>();
            let mut input = (0..l).collect::<Vec<u64>>();
            time(|| vec_rotate_juggle(&mut input.clone(), i));
            time(|| vec_rotate_swap(&mut input.clone()[..], i));
            time(|| vec_rotate_reverse(&mut input.clone()[..], i));
            vec_rotate_reverse(&mut input, i);
            //println!("{input:?}");
        },
        6 => {
            let in_path = args.get(0).expect("Expected path for name directory");
            let dir = lesk_directory(Path::new(&in_path)).expect("Could not build directory");
            let result = lesk_duplicates(&dir).unwrap();
            if let Some(name) = args.get(1) {
                dbg!(&dir[name]);
            }
            println!("{result:?}");
            //let name = args_iter.next().expect("Expected name to look up");
        }
        _ => panic!("Unknown/not implemented problem {}", problem),
    }
}

/*
4: reverse is ~twice as fast as juggle which is faster than swap if i << length, otherwise slower.
   reverse is better still if using built-in reverse...
   my swap stack overflows if shifting by a small amount...
7:

9: binary search = An log n + iB log n = (An + iB)log n
   linear search = iCn
   break even at
    iCn = (An + iB)log n
    iCn/(An + iB) = log n
    2^(in/(n+i)) = n
    C = (A/i + B/n)log n

10: fill shell with water...

*/
