use std::str;
use std::iter::{zip, once, repeat};
use std::ops::Sub;

use ansi_term::Style;
use lazy_static::lazy_static;
use itertools::{chain, Itertools};

lazy_static! {
    static ref BANDS: Vec<u64> = vec![
        2200,
        2700,
        3200,
        3700,
        4200,
        // ...
    ];

    static ref RATES: Vec<f32> = vec![
        0.0,
        0.14,
        0.15,
        0.16,
        0.17,
        // ...
    ];
    static ref BANDS_LOWHIGH: Vec<(u64, u64)> = zip( chain(once(&0), BANDS.iter()), chain(BANDS.iter(), once(&u64::MAX)))
        .map(|(&low, &high)| (low, high))
        .collect();

    static ref PREV_CUMULATIVE: Vec<u64> = {
        let mut result = vec![];
        let mut acc = 0.0;

        for ((low, high), rate) in zip(BANDS_LOWHIGH.iter(), RATES.iter()) {
            result.push(acc);
            acc += (high - low) as f32 * rate;
        }
        result.iter().map(|&x| x as u64).collect()
    };
}

fn tax(income: u64) -> u64 {
    let (i, (low, _)) = BANDS_LOWHIGH
        .iter()
        .enumerate()
        .find(|(_, (_, high))| income <= *high)
        .unwrap();

    let mut result = ((income - low) as f32 * RATES[i]) as u64;
    result += PREV_CUMULATIVE[i];
    result

    /* with a long if chain, changing the a tax-band or rate requires changing the corresponding "if" and every one after it */
}

fn recur(iters: usize, input: &[f64], coeffs: &[f64]) -> f64 {
    let next = zip(input.iter(), coeffs.iter()).map(|(&x, &y)| y * x).sum();
    if iters == 0 {
        return next;
    } else {
        return recur(iters - 1, &[&input[1..], &[next]].concat(), coeffs);
    }

    /* without arrays you'd need to manually cycle the inputs through k variables */
}

const MONTH_LENGTHS: [u8; 12] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const LY_MONTH_LENGTHS: [u8; 12] = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const WEEKDAYS: [&'static str; 7] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
// weekday of 1st of January, 1 AD: Monday
const EPOCH_WEEKDAY: u8 = 0;

fn is_leap_year(year: i32) -> bool {
    year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
}

struct Date {
    day: u32,
    month: u32,
    year: i32,
}

impl Date {
    fn days_since_epoch(&self) -> i64 {
        let _year = self.year - 1;
        (_year * 365 + _year / 4 - _year / 100 + _year / 400) as i64 +
            if is_leap_year(self.year) {
                LY_MONTH_LENGTHS[..self.month as usize - 1].iter().map(|&n| n as i64).sum::<i64>()
            } else {
                MONTH_LENGTHS[..self.month as usize - 1].iter().map(|&n| n as i64).sum::<i64>()
            } +
            (self.day - 1) as i64
    }

    fn month_length(&self) -> u8 {
        if is_leap_year(self.year) {
            LY_MONTH_LENGTHS[self.month as usize - 1]
        } else {
            MONTH_LENGTHS[self.month as usize - 1]
        }
    }

    fn weekday(&self) -> u8 {
        ((EPOCH_WEEKDAY as i64 + self.days_since_epoch().abs()) % 7) as u8
    }
}

impl Sub for Date {
    type Output = i64;
    fn sub(self, other: Self) -> Self::Output {
        self.days_since_epoch() - other.days_since_epoch()
    }
}

fn parse_date(date: &String) -> Date {
    let parts = date.split('-');
    let mut int_parts = parts.map(|part| part.parse::<i64>().expect(format!("Invalid integer: {part}").as_str()));

    let result = Date{
        year: int_parts.next().expect("Invalid date: no year").try_into().unwrap(),
        month: int_parts.next().expect("Invalid date: no month").try_into().unwrap(),
        day: int_parts.next().expect("Invalid date: no day").try_into().unwrap(),
    };

    assert!(result.year > 0);
    assert!(result.month > 0);
    assert!(result.day > 0);

    assert!(result.month <= 12);

    let max_day = if is_leap_year(result.year) {
        LY_MONTH_LENGTHS[result.month as usize - 1]
    } else {
        MONTH_LENGTHS[result.month as usize - 1]
    };
    assert!(result.day <= max_day.into());

    result
}

fn days(a: &String, b: &String) -> i64 {
    let (parsed_a, parsed_b) = (parse_date(a), parse_date(b));
    parsed_b - parsed_a
}

fn weekday(a: &String) -> &'static str {
    WEEKDAYS[parse_date(a).weekday() as usize]
}

fn calendar(a: &String) -> String {
    let date = parse_date(a);
    let month_start = Date{year: date.year, month: date.month, day: 1}.weekday();
    let month_length = date.month_length();

    let mut weekdays_abbr = WEEKDAYS.iter().map(|w| &w[0..2]);
    let days = (1..=month_length)
        .map(|i| if i as u32 == date.day {
            Style::new().bold().paint(format!("{:>2} ", i)).to_string()
        } else {
            format!("{:>2} ", i)
        })
        .collect::<Vec<_>>();
    let first_days = &days[..7 - month_start as usize];
    let tmp = &mut days[7 - month_start as usize..]
        .iter()
        .chunks(7);
    let mut tail_days = tmp
        .into_iter()
        .map(|mut chunk| chunk.join(""))
        .intersperse("\n".to_string());

    format!(
        "{}\n{}{}\n{}",
        weekdays_abbr.join(" "),
        repeat("   ").take(month_start.into()).join(""),
        first_days.join(""),
        tail_days.join(""),
    )
}

lazy_static! {
    static ref HYPHENATION_RULES: Vec<(&'static str, &'static[usize])> = vec![
        ("etic", &[2]),
        ("alistic", &[2, 4]),
        ("stic", &[3]),
        ("hnic", &[3]),
        ("nic", &[2]),
    ];
}

fn hyphenate(word: &String) -> Vec<String> {
    for (suffix, positions) in HYPHENATION_RULES.iter() {
        if word.ends_with(suffix) {
            let mut result = vec![];
            let len = word.len();
            for &pos in positions.iter() {
                result.push(format!("{}-{}",
                    str::from_utf8(&word.as_bytes()[..len-pos]).unwrap(),
                    str::from_utf8(&word.as_bytes()[len-pos..]).unwrap())
                );
            }
            return result;
        }
    }
    vec![word.clone()]
}

pub fn main(problem: u32, args: &Vec<String>) {
    match problem {
        1 => {
            let income = args.get(0).expect("Expected income amount").parse::<u64>().unwrap();
            println!("{}", tax(income));
        }
        2 => {
            let k = args.get(0).expect("Expected order of recurrence relation").parse::<usize>().unwrap();
            let input = args.get(1..k + 1).expect(format!("Expected {k} input values").as_str())
                .iter()
                .map(|s| s.parse::<f64>().unwrap())
                .collect::<Vec<f64>>();
            let coeffs = args.get(k + 1..2 * k + 1).expect(format!("Expected {k} coefficient values").as_str())
                .iter()
                .map(|s| s.parse::<f64>().unwrap())
                .collect::<Vec<f64>>();
            let m = args.get(2 * k + 1).expect("Expected number of iterations").parse::<usize>().unwrap();

            println!("{}", recur(m, &input, &coeffs));
        }
        4 => {
            match args.get(0).expect("Expected type of operation").as_str() {
                "days" => {
                    let a = args.get(1).expect("Expected date a");
                    let b = args.get(2).expect("Expected date b");
                    println!("{}", days(a, b));
                },
                "weekday" => {
                    println!("{}", weekday(args.get(1).expect("Expected date")));
                },
                "calendar" => {
                    println!("{}", calendar(args.get(1).expect("Expected date")));
                },
                _ => {panic!("No such operation")}
            }
        }
        5 => {
            let word = args.get(0).expect("Expected a word");
            println!("{}", hyphenate(word).join("\n"));
        }
        _ => panic!("Unknown problem"),
    }
}
