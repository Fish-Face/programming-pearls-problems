use std::env;

// use clap::{Arg, App};

use pearls::ch1;
use pearls::ch2;
use pearls::ch3;
use pearls::ch4;
use pearls::ch5;

fn main() {
    // let args: Vec<String> = std::env::args().collect();
    // let chapter = args.at(1).expect("no chapter provided").parse::<u32>().expect("chapter invalid");
    // let problem = args.at(2).expect("no problem provided").parse::<u32>().expect("problem invalid");
    let chapter = env::args().nth(1).expect("no chapter provided").parse::<u32>().expect("chapter invalid");
    let problem = env::args().nth(2).expect("no problem provided").parse::<u32>().expect("problem invalid");
    let args: Vec<String> = std::env::args().skip(3).collect();
    match chapter {
        1 => ch1::main(problem, &args),
        2 => ch2::main(problem, &args),
        3 => ch3::main(problem, &args),
        4 => ch4::main(problem, &args),
        _ => panic!("Unknown chapter: {}", chapter),
    }
}
