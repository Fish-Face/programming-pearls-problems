use std::cmp::min;

use itertools::Itertools;
use rand::prelude::*;

use crate::utils::benchmark;

/*
 * Non-code Problems
 *
 * 1. Ruling out run-time errors:
 *    - division by zero: we only divide by constant 2
 *    - integer overflow: is actually possible if N is > INT_MAX/2, when computing m.
 *    - array out-of-bounds: if l <= u then (l + u) / 2 <= u. Hence if u is in bounds, so is m.
 *      in case a, we set l to m+1 which could be u+1 and hence could be out of bounds. But next
 *      iteration we immediately check if l > u and terminate if so.
 *    Formal proof: no thanks
 * 5. Step 1: search wikipedia for "Collatz conjecture"
 *    Step 2: confirm that it is still unsolved
 * 6. It always terminates because every step you reduce the number of beans by 1.
 * 7. Binary search with f_i(x) instead of a[i]. Instead of the stopping condition being finding
 *    the exact value, stop when l+1 = u.
 */

fn first_bin_search(a: &[u64], x: u64) -> Option<usize> {
    if a.is_empty() {
        return None;
    }
    let mut l = 0;
    let mut u = a.len() - 1;
    let mut m = 0;
    while l+1 <= u {
        // inv1: if x in a then x in a[l..=u]
        // inv2: if x in a[l..=u] then x not in a[0..<l]
        m = (l + u) / 2;
        if a[m] < x {
            l = m + 1;
            // inv2: a[m] < x implies a[m] not in a[0..=m] = a[0..<l]
        } else {
            u = m;
            // inv1: a[m] >= x implies x not in x[m+1..N] hence in x[l..=m] = x[l..=u]
            // inv2: l unchanged
        }
    }

    if a[m] == x {
        Some(m)
    } else if m < a.len() - 1 && a[m+1] == x {
        Some(m+1)
    } else {
        None
    }
}

fn bin_search(a: &[u64], x: u64) -> Option<usize> {
    if a.is_empty() {
        return None;
    }
    // initial: a is sorted
    // exit: a has length 1
    // recursion: test on midpoint and call on one half
    // well-foundedness: length decreases when halving

    // complexity:
    //   we do a constant number of comparisons per call (at most 2 here, at most 2 below [if we
    //   eliminate the last case], at most 3 in total)
    //   each call is on an array of size length/2 (rounded down, so the real division is an upper bound)
    //   the recursion terminates at worst on reaching length 1, so the depth is at most log_2 N
    //   this is constant * log_2 N comparisons at worst
    if a.len() == 1 {
        if a[0] == x {
            return Some(0);
        } else {
            return None;
        }
    }

    let m = (a.len() - 1) / 2;
    // len >= 2 => m < len => well-founded

    if a[m] == x {
        return Some(m);
    } else if a[m] < x {
        return bin_search(&a[m+1..], x).map(|i| i + m + 1);
    } else if a[m] > x {
        return bin_search(&a[..m], x);
    }

    panic!("unreachable");
}

fn orig_bin_search(a: &[u64], x: u64) -> Option<usize> {
    if a.is_empty() {
        return None;
    }
    let mut l = 0;
    let mut u = a.len() - 1;
    let mut m = 0;
    while l <= u {
        // inv1: if x in a then x in a[l..=u]
        m = (l + u) / 2;
        if a[m] == x {
            return Some(m);
        } else if a[m] < x {
            l = m + 1;
        } else {
            u = m - 1;
        }
    }

    None
}

const CACHE_LINE_SIZE: usize = 8;

fn faster_bin_search(a: &[u64], x: u64) -> Option<usize> {
    if a.is_empty() {
        return None;
    }
    let mut l = 0;
    let mut u = a.len() - 1;
    let mut m = 0;
    while l + CACHE_LINE_SIZE <= u {
        // inv1: if x in a then x in a[l..=u]
        m = (l + u) / 2;
        if a[m] == x {
            return Some(m);
        } else
        if a[m] < x {
            l = m + 1;
        } else {
            u = m - 1;
        }
    }

    for i in l..min(l + CACHE_LINE_SIZE, a.len() - 1) {
        if a[i] == x {
            return Some(i);
        }
    }

    None
}

fn is_sorted<I>(data: I) -> bool
where
    I: IntoIterator,
    I::Item: Ord + Clone
{
    data.into_iter().tuple_windows().all(|(x, y)| x <= y)
}

pub fn main(problem: u32, args: &Vec<String>) {
    match problem {
        2 => {
            let x = args.get(0).expect("Need a value to search for").parse::<u64>().expect("Couldn't parse target as u64");
            let mut a = args.get(1..).expect("Need one or more values to search").iter().map(
                |s| s.parse::<u64>().expect("Couldn't parse value as u64")
            ).collect::<Vec<_>>();
            assert!(is_sorted(a.iter()), "Input must be sorted");
            println!("{:?}", first_bin_search(&a, x));
        },
        3 => {
            let x = args.get(0).expect("Need a value to search for").parse::<u64>().expect("Couldn't parse target as u64");
            let mut a = args.get(1..).expect("Need one or more values to search").iter().map(
                |s| s.parse::<u64>().expect("Couldn't parse value as u64")
            ).collect::<Vec<_>>();
            assert!(is_sorted(a.iter()), "Input must be sorted");
            println!("{:?}", bin_search(&a, x));
        },
        8 => {
            let mut rng = rand::thread_rng();
            let max = 100_000;
            let a: Vec<u64> = (0..max).map(|i| i * 2).collect();
            let x: u64 = rng.gen_range(0..max * 2);
            println!("{:?}", benchmark(|| rng.gen_range(0..max * 2)));
            println!("{:?}", benchmark(|| orig_bin_search(&a, rng.gen_range(0..max * 2))));
            println!("{:?}", benchmark(|| faster_bin_search(&a, rng.gen_range(0..max * 2))));
        },
        _ => panic!("Unknown/not implemented problem {}", problem),
    }
}
