#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char *argv[])
{
    volatile int a = atoi(argv[1]);
	int b = atoi(argv[2]);

    {
    clock_t start = clock();
    volatile int result;
    for (int i = 0; i < 1'000'000'000; ++i) {
        result = a % b;
    }
    clock_t end = clock();
    printf("Modulo result: %d, time: %f\n", result, (float)(end - start) / CLOCKS_PER_SEC);
    }

    {
    clock_t start = clock();
    for (int i = 0; i < 1'000'000'000; ++i) {
        while (a >= b) {
            a -= b;
        }
    }
    clock_t end = clock();
    printf("while loop result: %d, time: %f\n", a, (float)(end - start) / CLOCKS_PER_SEC);
    }

	return 0;
}
