#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))

float *x;

float arrmax(int n) {
    if (n == 1) {
        return x[0];
    }
    // return max(x[n-1], arrmax(n-1));
    // 2 calls. Each call descends n-1 times for a total of 2^n-1 recursive calls
    return (
        ((x[n-1]) > (arrmax(n-1))) ?
            (x[n-1]) :
            (arrmax(n-1))
        );
}

int main(int argc, char *argv[])
{
    int a = atoi(argv[1]);
    x = (float*)malloc(a * sizeof(float));
    for (int i = 0; i < a; ++i) {
        // x[i] = rand() / (float)RAND_MAX;
        x[i] = i;
    }

    {
        clock_t start = clock();
        int result;
        for (int i = 0; i < 1'000'000; ++i) {
            result = arrmax(a);
        }
        clock_t end = clock();
        printf("result: %d, time: %f\n", result, (float)(end - start) / CLOCKS_PER_SEC);
    }

	return 0;
}
