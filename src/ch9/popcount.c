#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cstring>

#include <popcntintrin.h>
#include <immintrin.h>

unsigned char *x;


int popcount_naive(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N; ++i) {
        acc += x[i] >> 0 & 1;
        acc += x[i] >> 1 & 1;
        acc += x[i] >> 2 & 1;
        acc += x[i] >> 3 & 1;
        acc += x[i] >> 4 & 1;
        acc += x[i] >> 5 & 1;
        acc += x[i] >> 6 & 1;
        acc += x[i] >> 7 & 1;
    }
    return acc;
}

int popcount(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N; ++i) {
        acc += __builtin_popcount(x[i]);
    }
    return acc;
}


int popcount_uint(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N / sizeof(unsigned int); ++i) {
        acc += __builtin_popcount(((unsigned int*)x)[i]);
    }
    return acc;
}

int popcount_ull(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N / sizeof(unsigned long long); ++i) {
        acc += _mm_popcnt_u64(((unsigned long long*)x)[i]);
    }
    return acc;
}

__m256i _avx2_count ( __m256i v) {
    __m256i lookup = _mm256_setr_epi8 (
        0 , 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4
    );
    __m256i low_mask = _mm256_set1_epi8 (0x0f );
    __m256i lo = _mm256_and_si256 (v , low_mask );
    __m256i hi = _mm256_and_si256 ( _mm256_srli_epi32(v, 4) , low_mask );
    __m256i popcnt1 = _mm256_shuffle_epi8 ( lookup , lo );
    __m256i popcnt2 = _mm256_shuffle_epi8 ( lookup , hi );
    __m256i total = _mm256_add_epi8 ( popcnt1 , popcnt2);
    __m256i shuffled = _mm256_sad_epu8 (total , _mm256_setzero_si256 () );
    return shuffled;
}

unsigned char lookup8bit[256] = {
	/* 0 */ 0, /* 1 */ 1, /* 2 */ 1, /* 3 */ 2,
	/* 4 */ 1, /* 5 */ 2, /* 6 */ 2, /* 7 */ 3,
	/* 8 */ 1, /* 9 */ 2, /* a */ 2, /* b */ 3,
	/* c */ 2, /* d */ 3, /* e */ 3, /* f */ 4,
	/* 10 */ 1, /* 11 */ 2, /* 12 */ 2, /* 13 */ 3,
	/* 14 */ 2, /* 15 */ 3, /* 16 */ 3, /* 17 */ 4,
	/* 18 */ 2, /* 19 */ 3, /* 1a */ 3, /* 1b */ 4,
	/* 1c */ 3, /* 1d */ 4, /* 1e */ 4, /* 1f */ 5,
	/* 20 */ 1, /* 21 */ 2, /* 22 */ 2, /* 23 */ 3,
	/* 24 */ 2, /* 25 */ 3, /* 26 */ 3, /* 27 */ 4,
	/* 28 */ 2, /* 29 */ 3, /* 2a */ 3, /* 2b */ 4,
	/* 2c */ 3, /* 2d */ 4, /* 2e */ 4, /* 2f */ 5,
	/* 30 */ 2, /* 31 */ 3, /* 32 */ 3, /* 33 */ 4,
	/* 34 */ 3, /* 35 */ 4, /* 36 */ 4, /* 37 */ 5,
	/* 38 */ 3, /* 39 */ 4, /* 3a */ 4, /* 3b */ 5,
	/* 3c */ 4, /* 3d */ 5, /* 3e */ 5, /* 3f */ 6,
	/* 40 */ 1, /* 41 */ 2, /* 42 */ 2, /* 43 */ 3,
	/* 44 */ 2, /* 45 */ 3, /* 46 */ 3, /* 47 */ 4,
	/* 48 */ 2, /* 49 */ 3, /* 4a */ 3, /* 4b */ 4,
	/* 4c */ 3, /* 4d */ 4, /* 4e */ 4, /* 4f */ 5,
	/* 50 */ 2, /* 51 */ 3, /* 52 */ 3, /* 53 */ 4,
	/* 54 */ 3, /* 55 */ 4, /* 56 */ 4, /* 57 */ 5,
	/* 58 */ 3, /* 59 */ 4, /* 5a */ 4, /* 5b */ 5,
	/* 5c */ 4, /* 5d */ 5, /* 5e */ 5, /* 5f */ 6,
	/* 60 */ 2, /* 61 */ 3, /* 62 */ 3, /* 63 */ 4,
	/* 64 */ 3, /* 65 */ 4, /* 66 */ 4, /* 67 */ 5,
	/* 68 */ 3, /* 69 */ 4, /* 6a */ 4, /* 6b */ 5,
	/* 6c */ 4, /* 6d */ 5, /* 6e */ 5, /* 6f */ 6,
	/* 70 */ 3, /* 71 */ 4, /* 72 */ 4, /* 73 */ 5,
	/* 74 */ 4, /* 75 */ 5, /* 76 */ 5, /* 77 */ 6,
	/* 78 */ 4, /* 79 */ 5, /* 7a */ 5, /* 7b */ 6,
	/* 7c */ 5, /* 7d */ 6, /* 7e */ 6, /* 7f */ 7,
	/* 80 */ 1, /* 81 */ 2, /* 82 */ 2, /* 83 */ 3,
	/* 84 */ 2, /* 85 */ 3, /* 86 */ 3, /* 87 */ 4,
	/* 88 */ 2, /* 89 */ 3, /* 8a */ 3, /* 8b */ 4,
	/* 8c */ 3, /* 8d */ 4, /* 8e */ 4, /* 8f */ 5,
	/* 90 */ 2, /* 91 */ 3, /* 92 */ 3, /* 93 */ 4,
	/* 94 */ 3, /* 95 */ 4, /* 96 */ 4, /* 97 */ 5,
	/* 98 */ 3, /* 99 */ 4, /* 9a */ 4, /* 9b */ 5,
	/* 9c */ 4, /* 9d */ 5, /* 9e */ 5, /* 9f */ 6,
	/* a0 */ 2, /* a1 */ 3, /* a2 */ 3, /* a3 */ 4,
	/* a4 */ 3, /* a5 */ 4, /* a6 */ 4, /* a7 */ 5,
	/* a8 */ 3, /* a9 */ 4, /* aa */ 4, /* ab */ 5,
	/* ac */ 4, /* ad */ 5, /* ae */ 5, /* af */ 6,
	/* b0 */ 3, /* b1 */ 4, /* b2 */ 4, /* b3 */ 5,
	/* b4 */ 4, /* b5 */ 5, /* b6 */ 5, /* b7 */ 6,
	/* b8 */ 4, /* b9 */ 5, /* ba */ 5, /* bb */ 6,
	/* bc */ 5, /* bd */ 6, /* be */ 6, /* bf */ 7,
	/* c0 */ 2, /* c1 */ 3, /* c2 */ 3, /* c3 */ 4,
	/* c4 */ 3, /* c5 */ 4, /* c6 */ 4, /* c7 */ 5,
	/* c8 */ 3, /* c9 */ 4, /* ca */ 4, /* cb */ 5,
	/* cc */ 4, /* cd */ 5, /* ce */ 5, /* cf */ 6,
	/* d0 */ 3, /* d1 */ 4, /* d2 */ 4, /* d3 */ 5,
	/* d4 */ 4, /* d5 */ 5, /* d6 */ 5, /* d7 */ 6,
	/* d8 */ 4, /* d9 */ 5, /* da */ 5, /* db */ 6,
	/* dc */ 5, /* dd */ 6, /* de */ 6, /* df */ 7,
	/* e0 */ 3, /* e1 */ 4, /* e2 */ 4, /* e3 */ 5,
	/* e4 */ 4, /* e5 */ 5, /* e6 */ 5, /* e7 */ 6,
	/* e8 */ 4, /* e9 */ 5, /* ea */ 5, /* eb */ 6,
	/* ec */ 5, /* ed */ 6, /* ee */ 6, /* ef */ 7,
	/* f0 */ 4, /* f1 */ 5, /* f2 */ 5, /* f3 */ 6,
	/* f4 */ 5, /* f5 */ 6, /* f6 */ 6, /* f7 */ 7,
	/* f8 */ 5, /* f9 */ 6, /* fa */ 6, /* fb */ 7,
	/* fc */ 6, /* fd */ 7, /* fe */ 7, /* ff */ 8
};

int popcount_AVX2_muwa2(unsigned char* x, int N) {
    size_t i = 0;

    const __m256i lookup = _mm256_setr_epi8(
        /* 0 */ 0, /* 1 */ 1, /* 2 */ 1, /* 3 */ 2,
        /* 4 */ 1, /* 5 */ 2, /* 6 */ 2, /* 7 */ 3,
        /* 8 */ 1, /* 9 */ 2, /* a */ 2, /* b */ 3,
        /* c */ 2, /* d */ 3, /* e */ 3, /* f */ 4,

        /* 0 */ 0, /* 1 */ 1, /* 2 */ 1, /* 3 */ 2,
        /* 4 */ 1, /* 5 */ 2, /* 6 */ 2, /* 7 */ 3,
        /* 8 */ 1, /* 9 */ 2, /* a */ 2, /* b */ 3,
        /* c */ 2, /* d */ 3, /* e */ 3, /* f */ 4
    );

    const __m256i low_mask = _mm256_set1_epi8(0x0f);

    __m256i acc = _mm256_setzero_si256();

#define ITER { \
        const __m256i vec = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(x + i)); \
        const __m256i lo  = _mm256_and_si256(vec, low_mask); \
        const __m256i hi  = _mm256_and_si256(_mm256_srli_epi16(vec, 4), low_mask); \
        const __m256i popcnt1 = _mm256_shuffle_epi8(lookup, lo); \
        const __m256i popcnt2 = _mm256_shuffle_epi8(lookup, hi); \
        local = _mm256_add_epi8(local, popcnt1); \
        local = _mm256_add_epi8(local, popcnt2); \
        i += 32; \
    }

    while (i + 8*32 <= N) {
        __m256i local = _mm256_setzero_si256();
        ITER ITER ITER ITER
        ITER ITER ITER ITER
        acc = _mm256_add_epi64(acc, _mm256_sad_epu8(local, _mm256_setzero_si256()));
    }

    __m256i local = _mm256_setzero_si256();

    while (i + 32 <= N) {
        ITER;
    }

    acc = _mm256_add_epi64(acc, _mm256_sad_epu8(local, _mm256_setzero_si256()));

#undef ITER

    int result = 0;

    result += (int)(_mm256_extract_epi64(acc, 0));
    result += (int)(_mm256_extract_epi64(acc, 1));
    result += (int)(_mm256_extract_epi64(acc, 2));
    result += (int)(_mm256_extract_epi64(acc, 3));

    for (/**/; i < N; i++) {
        result += lookup8bit[x[i]];
    }

    return result;
}

int popcount_avx2_muwa(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N; i += 32) {
        __m256i result = _avx2_count(_mm256_loadu_si256((__m256i*)&x[i]));
        acc += _mm256_extract_epi64(result, 0) +
            _mm256_extract_epi64(result, 1) +
            _mm256_extract_epi64(result, 2) +
            _mm256_extract_epi64(result, 3);
    }
    return acc;
}

void CSA( __m256i *h , __m256i *l , __m256i a , __m256i b , __m256i c) {
    __m256i u = _mm256_xor_si256(a, b);
    *h = _mm256_or_si256(_mm256_and_si256(a, b), _mm256_and_si256(u, c));
    *l = _mm256_xor_si256(u , c);
}

int avx_hs (unsigned char *x , int N ) {
    __m256i *d = (__m256i *) x;
    int size = N / 32;

    __m256i total = _mm256_setzero_si256 () ;
    __m256i ones = _mm256_setzero_si256 () ;
    __m256i twos = _mm256_setzero_si256 () ;
    __m256i fours = _mm256_setzero_si256 () ;
    __m256i eights = _mm256_setzero_si256 () ;
    __m256i sixteens = _mm256_setzero_si256 () ;
    __m256i twosA, twosB, foursA, foursB, eightsA, eightsB;
    for ( int i = 0; i < size ; i += 16) {
        CSA (&twosA, &ones, ones, d[i], d[i +1]) ;
        CSA (&twosB, &ones, ones, d[i +2], d[i +3]) ;
        CSA (&foursA, &twos, twos, twosA, twosB );
        CSA (&twosA, &ones, ones, d[i +4], d[i +5]) ;
        CSA (&twosB, &ones, ones, d[i +6], d[i +7]) ;
        CSA (&foursB, &twos, twos, twosA, twosB );
        CSA (&eightsA, &fours, fours, foursA, foursB );
        CSA (&twosA, &ones, ones, d[i +8], d[i +9]) ;
        CSA (&twosB, &ones, ones, d[i +10], d[i +11]) ;
        CSA (&foursA, &twos, twos, twosA, twosB );
        CSA (&twosA, &ones, ones, d[i +12], d[i +13]) ;
        CSA (&twosB, &ones, ones, d[i +14], d[i +15]) ;
        CSA (&foursB, &twos, twos, twosA, twosB );
        CSA (&eightsB, &fours, fours, foursA, foursB );
        CSA (&sixteens, &eights, eights, eightsA, eightsB);
        total = _mm256_add_epi64 (total, _avx2_count(sixteens));
    }
    total = _mm256_slli_epi64 ( total, 4) ;
    total = _mm256_add_epi64 ( total,
    _mm256_slli_epi64 ( _avx2_count ( eights ), 3) );
    total = _mm256_add_epi64 ( total,
    _mm256_slli_epi64 ( _avx2_count ( fours ), 2) );
    total = _mm256_add_epi64 ( total,
    _mm256_slli_epi64 ( _avx2_count ( twos ), 1) );
    total = _mm256_add_epi64 ( total, _avx2_count ( ones ));
    return _mm256_extract_epi64 ( total, 0)
    + _mm256_extract_epi64 ( total, 1)
    + _mm256_extract_epi64 ( total, 2)
    + _mm256_extract_epi64 ( total, 3) ;
}

int popcount_adder_tree(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N / sizeof(unsigned int); ++i) {
        unsigned long int r = ((unsigned int *)x)[i];
        r = (r & 0x5555555555555555 ) + (( r >> 1) & 0x5555555555555555 );
        r = (r & 0x3333333333333333 ) + (( r >> 2) & 0x3333333333333333 );
        r = (r & 0x0F0F0F0F0F0F0F0F ) + (( r >> 4) & 0x0F0F0F0F0F0F0F0F );
        r = (r & 0x00FF00FF00FF00FF ) + (( r >> 8) & 0x00FF00FF00FF00FF );
        r = (r & 0x0000FFFF0000FFFF ) + (( r >> 16) & 0x0000FFFF0000FFFF );
        acc += (r & 0x00000000FFFFFFFF ) + (( r >> 32) & 0x00000000FFFFFFFF );
    }
    return acc;
}

int popcount_bin_char(unsigned char *x, int N) {
    int table[256];
    for (int i = 0; i < 256; i++) {
        table[i] = __builtin_popcount(i);
    }
    int acc = 0;
    for (int i = 0; i < N; ++i) {
        acc += table[x[i]];
    }
    return acc;
}

int short_table[(1 << sizeof(short) * 8)];

int popcount_bin_short(unsigned char *x, int N) {
    int acc = 0;
    for (int i = 0; i < N / sizeof(unsigned short); ++i) {
        acc += short_table[((unsigned short *)x)[i]];
    }
    return acc;
}


void eval(int (*f)(unsigned char *, int), const char* fname, unsigned char *x, int N) {
    clock_t start = clock();
    int result;
    for (int i = 0; i < 10'000; i++) {
        result = f(x, N);;
    }
    clock_t end = clock();
    int pad_amount = 30 - (int)strlen(fname);
    char pad[30] = "                             ";
    pad[pad_amount] = '\0';
    printf("%s: %d,%stime: %f\n", fname, result, pad, (float)(end - start) / CLOCKS_PER_SEC);
}

#define EVAL(f) eval(&f, #f, x, N);

int main(int argc, char *argv[])
{
    int N = atoi(argv[1]);
    x = (unsigned char*)malloc(N);

    clock_t start = clock();
    for (unsigned int i = 0; i < 1 << sizeof(short) * 8; i++) {
        short_table[i] = __builtin_popcount(i);
    }
    clock_t end = clock();
    printf("lookup table init time: %f\n", (float)(end - start) / CLOCKS_PER_SEC);

	srand(time(0));
	for (int i = 0; i < N; ++i) {
		x[i] = 0x55;
	}

	EVAL(popcount_naive);
	EVAL(popcount);
	EVAL(popcount_uint);
	EVAL(popcount_ull);
	// EVAL(popcount_128);
	// EVAL(popcount_avx2_muwa);
	EVAL(popcount_AVX2_muwa2);
	// EVAL(avx_hs);
	EVAL(popcount_adder_tree);
	EVAL(popcount_bin_char);
	EVAL(popcount_bin_short);

	return 0;
}
