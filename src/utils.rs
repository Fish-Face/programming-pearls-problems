use std::time::{Instant, Duration};

pub fn time<F: Fn() -> T, T>(f: F) -> T {
    let start = Instant::now();
    let ret = f();
    println!("Elapsed time: {:.2?}", start.elapsed());
    ret
}

#[derive(Debug, Clone)]
pub struct Stats {
    min: Duration,
    max: Duration,
    avg: Duration,
    // std: Duration,
    iters: usize,
}

pub fn benchmark<F: FnMut() -> T, T>(mut f: F) -> Stats {
    const max_time: Duration = Duration::from_secs(10);
    const max_iters: usize = 1000000;

    let mut times = Vec::with_capacity(max_iters);

    let start = Instant::now();
    let mut t = Instant::now();

    let mut i = 0;
    while start.elapsed() < max_time && i < max_iters {
        i += 1;
        t = Instant::now();
        f();
        times.push(t.elapsed());
    }

    Stats{
        min: *times.iter().min().unwrap(),
        max: *times.iter().max().unwrap(),
        avg: start.elapsed() / i as u32,
        iters: i,
    }
}
