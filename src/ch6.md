# Problems

## Assume that computers are now 1000 times faster. How much bigger can N be while remaining at a 1 day runtime for an O(N^2) vs O(N log N) algorithm?

For a quadratic algorithm, the problem size will increase in proportion to the square-root of the speed-up; for 1000 this is about 32x.

For an N log N algorithm, the problem size can increase by a factor of about 190.

## Discuss speed-ups at various levels and the dependencies between them for:

### Factoring 500-digit numbers

* You'll need bignums, so store them as bignums rather than something silly like strings
* Use a sieve or better algorithm
* Your bignum library probably already optimises the operations but if not you'd look at that

### Fourier analysis

* Just use FFTW
* Order your data correctly
* Optimise the hop size

### Simulating VLSI circuits

???

### Searching a text file for strings

* Store an index of partial string -> location
* Consider what kind of string needs to be searched for to optimise the index; use the smallest character set possible
* Index entire words maybe

## Test float64 -> float32

* We done this?

## Discuss attacking the following at different design levels

### 

## 

## Is efficiency secondary to correctness?

Yes, as long as correctness is relative to the problem that actually needs solving. If you need 1mm precision and your efficiency improvements cause a 0.5mm deviation, it doesn't matter.
