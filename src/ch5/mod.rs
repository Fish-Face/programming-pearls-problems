use itertools::Itertools;

trait Mergeable {
    fn merge(items: impl IntoIterator<Item = Self>) -> Self;
}

struct Chunk {
    data: Vec<u64>
}

impl Mergeable for Chunk {
    fn merge(items: impl IntoIterator<Item = Self>) -> Self {
        // let mut acc: Vec<int> = Vec::new();
        Chunk{data: items.into_iter().map(|c| {c.data}).concat()}
    }
}
